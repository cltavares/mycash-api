package com.bootcamp.mycash.domain;

public enum UsuarioRole {

	/*O underline é importante para o spring security*/
	ROLE_ADMIN,
	ROLE_USER
	
}