package com.bootcamp.mycash.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.mycash.domain.Lancamento;

public interface LancamentoRepository extends JpaRepository<Lancamento, Integer> {

}
