package com.bootcamp.mycash.web.api;

import java.beans.Beans;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.mycash.domain.Lancamento;
import com.bootcamp.mycash.repository.LancamentoRepository;
import com.bootcamp.mycash.service.LancamentoService;



@RestController
@RequestMapping("/api/lancamento")
public class LancamentoController {
	
	/*
	@GetMapping
	public List<Lancamento> todos(){
		
		Lancamento lancamento = new Lancamento();
		lancamento.setDescricao("empada");
		lancamento.setValor(5.5);
		lancamento.setData(LocalDate.now());
		
		return Arrays.asList(lancamento);  //Collections.EMPTY_LIST;
	}
	
	@GetMapping("/{id}")
	public Lancamento apenasUm(@PathVariable("id") Integer id ) {
		return null;
	}
	*/
	
	@Autowired
	private LancamentoService service;
	
	// Somente exemplo
	private static List<Lancamento> lancamentos = new ArrayList<>();
	
	@GetMapping
	Page<Lancamento> todos(Pageable pageable) {
		return service.todos(pageable);
	}
	
	@GetMapping("/{id}")
	public Lancamento apenasUm(@PathVariable("id") Integer id ) {
		return service.apenasUm(id);
	}
	
	@PostMapping	
	Lancamento criar(@Valid @RequestBody Lancamento lancamento) {

		return service.criar(lancamento);
	}
	
	@PutMapping("/{id}")
	Lancamento atualizar(
			@PathVariable Integer id, 
			@RequestBody Lancamento novoLancamento) {
	
		return service.atualizar(id, novoLancamento);
	}
	
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable Integer id) {
		service.deleteById(id);		
	}
	
	/*
	@PostMapping	
	Lancamento criar2(@RequestBody Lancamento lancamento) {
		lancamentos.add(lancamento);
		return lancamento;
	}
	
	@GetMapping("/{id}")
	Lancamento apenasUm2(@PathVariable Integer id) {
		Optional<Lancamento> lancamentoOp = buscaPorId(id);
		
		return lancamentoOp.get();
	}
	
	@PutMapping("/{id}")
	Lancamento atualizar(
			@PathVariable Integer id, 
			@RequestBody Lancamento novoLancamento) {
	
		return buscaPorId(id)
			.map(lancamento -> {
				lancamento.setDescricao(novoLancamento.getDescricao());
				lancamento.setValor(novoLancamento.getValor());
				lancamento.setTipo(novoLancamento.getTipo());
				lancamento.setData(novoLancamento.getData());
				
				lancamentos.remove(lancamento);
				lancamentos.add(lancamento);
				
				return lancamento;
			})
			.orElseThrow(() -> new EntityNotFoundException());
	}
	
	@DeleteMapping("/{id}")
	void excluir(@PathVariable Integer id) {
		buscaPorId(id)
		.map(lancamento -> {
			lancamentos.remove(lancamento);
			
			return lancamento;
		})
		.orElseThrow(() -> new EntityNotFoundException());
	}

	private Optional<Lancamento> buscaPorId(Integer id) {
		Optional<Lancamento> lancamentoOp = lancamentos
				.stream()
				.filter(l -> l.getId() == id)
				.reduce((u, v) -> { throw new IllegalStateException(); });
		return lancamentoOp;
	}
	*/
	

}
