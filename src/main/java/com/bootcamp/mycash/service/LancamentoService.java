package com.bootcamp.mycash.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.bootcamp.mycash.domain.Lancamento;
import com.bootcamp.mycash.repository.LancamentoRepository;

@Service
public class LancamentoService {

	@Autowired
	private LancamentoRepository repo;
	
	public Page<Lancamento> todos(Pageable pageable) {
		return repo.findAll(pageable);
	}

	public Lancamento criar(Lancamento lancamento) {
		return repo.save(lancamento);
	}

	public Lancamento apenasUm(Integer id) {
		return repo.findById(id)
				.orElseThrow(() -> new EntityNotFoundException());
	}

	public Lancamento atualizar(
			Integer id, 
			Lancamento novoLancamento) {
		
		return repo.findById(id)
			.map(lancamento -> {
				lancamento.setDescricao(novoLancamento.getDescricao());
				lancamento.setValor(novoLancamento.getValor());
				lancamento.setTipo(novoLancamento.getTipo());
				lancamento.setData(novoLancamento.getData());
				
				//BeanUtils.copyProperties(novoLancamento, lancamento,"id");
				
				return repo.save(lancamento);
			})
			.orElseThrow(() -> new EntityNotFoundException());
		
	}

	public void deleteById(Integer id) {
		repo.deleteById(id);
	}

}
